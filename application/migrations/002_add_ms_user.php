<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_ms_user extends CI_Migration {

    public function up() {
        echo "Start Master User Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
			'profile_id' => array(
				'type' => 'INT',
				'constraint' => 4,
				'unsigned' => TRUE
			),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'password_hash' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('ms_user')) {
            echo "Creating foreign key for profile_id \n";
			$this->db->query('ALTER TABLE `ms_user` ADD KEY `idx-ms_user-profile_id` (`profile_id`)');
            $this->db->query('ALTER TABLE `ms_user` ADD CONSTRAINT `fk-ms_user-profile_id` FOREIGN KEY (`profile_id`) REFERENCES `ms_profile` (`id`)');
            echo "Status : Success \n";
            
            echo "Creating help for 'password_hash'\n";
            $this->db->query('ALTER TABLE `ms_user` MODIFY COLUMN `password_hash` INT(1) NOT NULL COMMENT "pake password_hash()"');
            
            echo "SUCCESS";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->dbforge->drop_table('ms_user');
    }
}
