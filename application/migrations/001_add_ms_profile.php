<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_ms_profile extends CI_Migration {

    public function up() {
        echo "Start Master Profile Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nama_depan' => array(
                'type' => 'VARCHAR',
                'constraint' => 30,
                'null' => FALSE
            ),
            'nama_belakang' => array(
                'type' => 'VARCHAR',
                'constraint' => 30,
                'null' => FALSE
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('ms_profile')) {
            echo "Status : Success \n";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->dbforge->drop_table('ms_profile');
    }

}
